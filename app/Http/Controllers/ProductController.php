<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products\StoreProductRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index(Request $request)
    {
        $products = $this->product->all();

        return view('product', compact('products'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreProductRequest $request)
    {
        $product = $this->product->updateOrCreate(['id' => $request->product_id], [
            'title' => $request->title,
            'content' => $request->content
        ]);

        return response()->json(['code' => 200, 'message' => 'Product Created successfully', 'data' => $product], 200);
    }

    public function show(Product $product)
    {
        return response()->json($product);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return response()->json(['success' => 'Product deleted successfully.']);
    }
}
